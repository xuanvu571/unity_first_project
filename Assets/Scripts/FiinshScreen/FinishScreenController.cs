using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FinishScreenController : MonoBehaviour
{
    public TMP_Text _scoreDisplay;
    // Start is called before the first frame update
    void Start()
    {
        _scoreDisplay.text = $"Your score: {GameController._score}";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
