using Assets.Scripts;
using Assets.Scripts.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public GameObject _obstacleObject;
    public GameObject _startText;
    public float _obstacleObjectInitAtY = 6;
    public float _obstacleObjectInitAtZ = 20;
    public GameObject _planeObject;
    float _planeWidth;
    bool _displayStartText = false;

    static public int _score = -1;
    static public bool _preventToUseEnergy = true;
    static public bool _startGame = false;

    // Start is called before the first frame update
    void Start()
    {
        _score = -1;
        _startGame = false;
        _preventToUseEnergy = true;
        _displayStartText = false;

        var planceSize = ObjectHelper.GetObjectSize(_planeObject);
        _planeWidth = planceSize.x;

        StartCoroutine(CreateObstacleAuto());
    }

    // Update is called once per frame
    void Update()
    {
        DisplayStartText();
    }

    void DisplayStartText()
    {
        if (_startGame && !_displayStartText)
        {
            Vector3 initPos = new Vector3(0, 0, 0);
            Instantiate(_startText, initPos, Quaternion.identity);
            _displayStartText = true;
        }
    }

    IEnumerator CreateObstacleAuto()
    {
        while (true)
        {
            yield return new WaitForSeconds(GameSettings._DELAY_SECOND_FOR_CREATING_OBSTACLES);
            if(GameController._startGame)
            {
                CreateObstacles();
            }
        }
    }

    void CreateObstacles()
    {
        var obstaclesList = ObstacleCreator.CreateObstacles(_planeWidth);
        foreach (var obstacle in obstaclesList)
        {
            InstantiateObstacle(obstacle);
        }

        IncreaseScore();
    }
    void InstantiateObstacle(Obstacle obstacle)
    {
        Vector3 initPos = new Vector3(obstacle.X, _obstacleObjectInitAtY, _obstacleObjectInitAtZ);
        GameObject cloneObstacleObject = Instantiate(_obstacleObject, initPos, Quaternion.identity);

        ObjectHelper.SetObjectWidth(cloneObstacleObject, obstacle.Width);
    }

    void IncreaseScore()
    {
        _score++;
    }
}
