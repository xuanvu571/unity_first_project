using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleObjectController : MonoBehaviour
{
    Rigidbody _rigidBody;
    bool _isOnTheFloor = false;

    // Start is called before the first frame update
    void Start()
    {
        _rigidBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_isOnTheFloor)
        {
            MoveObstacleToTheMainObject();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "PlaneTag")
        {
            _isOnTheFloor = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "PlaneTag")
        {
            StartCoroutine(DestroyObject());
        }
    }

    IEnumerator DestroyObject()
    {
        yield return new WaitForSeconds(1.0f);
        Destroy(this.gameObject);
    }

    private void MoveObstacleToTheMainObject()
    {
        _rigidBody.velocity = Vector3.back * 20f;
    }
}
