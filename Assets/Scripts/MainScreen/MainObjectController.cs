using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainObjectController : MonoBehaviour
{
    public GameObject _mainCamera;
    public float _speed = 50f;
    public float _maxWidth = 5f;
    public float _minWidth = 1f;
    public float _widthScale = 0.1f;

    float _currentWidth;
    Rigidbody _rigidBody;
    bool _endGame = false;

    // Start is called before the first frame update
    void Start()
    {
        _rigidBody = GetComponent<Rigidbody>();
        _currentWidth = _maxWidth;
        _endGame = false;

        ObjectHelper.SetObjectWidth(this.gameObject, _currentWidth);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameController._startGame == false)
        {
            if(_startToShowIntroduce)
                ShowIntroduce();
        }
        else
        {
            MoveObjectWithKey();
            TransformObjectWithKey();

            if (_endGame)
            {
                _mainCamera.transform.LookAt(this.transform);
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "PlaneTag")
        {
            TouchPlane();
        }
        if (collision.gameObject.tag == "ObstacleTag")
        {
            // Destroy(collision.gameObject);
            TouchObtacles();
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "PlaneTag")
        {
            EndTheGame();
        }
    }

    private void MoveObjectWithKey()
    {
        float xInput = Input.GetAxis("Horizontal");
        //float zInput = Input.GetAxis("Vertical");

        _rigidBody.AddForce(xInput * _speed, 0, 0);
    }

    private void TransformObjectWithKey()
    {
        if (Input.GetKey(GameSettings._KEY_FOR_SHRINK) && !GameController._preventToUseEnergy)
        {
            ShrinkObject();
        }
        else
        {
            UnshrinkObject();
        }
    }

    private void ShrinkObject()
    {
        _currentWidth = _currentWidth - (_currentWidth * _widthScale);
        if (_currentWidth <= _minWidth) 
            _currentWidth = _minWidth;

        ObjectHelper.SetObjectWidth(this.gameObject, _currentWidth);
    }

    private void UnshrinkObject()
    {
        //_currentWidth = _currentWidth + (_currentWidth * _widthScale);
        //if (_currentWidth >= _maxWidth) 
        //    _currentWidth = _maxWidth;
        _currentWidth = _maxWidth;

        ObjectHelper.SetObjectWidth(this.gameObject, _currentWidth);
    }

    private void TouchObtacles()
    {
        _rigidBody.velocity = Vector3.back * 10f;
    }
    private void EndTheGame()
    {
        _endGame = true;
        StartCoroutine(LoadFinishScreen());
    }
    IEnumerator LoadFinishScreen()
    {
        yield return new WaitForSeconds(3.0f);
        //Destroy(this.gameObject);
        SceneManager.LoadScene("FinishScreen");
    }

    bool _startToShowIntroduce = false;
    int _introShrink = 0;
    int _introUnshrink = 0;
    int _maxTimeIntroShrinkUnshrinkAvalibitity = 4;
    int _introMoveLeft = 0;
    int _introMoveRight = 0;
    int _maxTimeIntroMovementAvalibitity = 1;
    private void TouchPlane()
    {
        StartCoroutine(StartShowIntroduce());
    }
    IEnumerator StartShowIntroduce()
    {
        yield return new WaitForSeconds(0.5f);
        _startToShowIntroduce = true;
    }
    private void ShowIntroduce()
    {
        if (_introShrink < _maxTimeIntroShrinkUnshrinkAvalibitity)
        {
            ShowShrinkAndUnshrinkAvalibility();
        }
        else if (_introMoveLeft < _maxTimeIntroMovementAvalibitity)
        {
            ShowMovementToLeft();
        }
        else if (_introMoveRight < _maxTimeIntroMovementAvalibitity)
        {
            ShowMovementToRight();
        }
        else
        {
            ShowMovementToLeft();
            if (transform.position.x < 3)
            {
                _rigidBody.AddForce(0, 0, 0);
                StartTheGame();
            }
        }
    }
    private void StartTheGame()
    {
        _startToShowIntroduce = false;
        GameController._startGame = true;
    }
    private void ShowShrinkAndUnshrinkAvalibility()
    {
        if (_introShrink == _introUnshrink)
        {
            ShrinkObject();
            if (_currentWidth == _minWidth)
                _introShrink++;
        }
        else
        {
            UnshrinkObject();
            if (_currentWidth == _maxWidth)
                _introUnshrink++;
        }
    }
    private void ShowMovementToLeft()
    {
        _rigidBody.AddForce(-_speed, 0, 0);
        if (transform.position.x < -5) _introMoveLeft++;
    }
    private void ShowMovementToRight()
    {
        _rigidBody.AddForce(_speed, 0, 0);
        if (transform.position.x > 4) _introMoveRight++;
    }
}
