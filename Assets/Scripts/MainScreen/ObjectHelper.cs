﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class ObjectHelper
    {
        public static void SetObjectWidth(GameObject theGameObject, float newSize)
        {
            float size = theGameObject.GetComponent<Renderer>().bounds.size.x;
            Vector3 rescale = theGameObject.transform.localScale;
            rescale.x = newSize * rescale.x / size;

            theGameObject.transform.localScale = rescale;
        }
        public static Vector3 GetObjectSize(GameObject theGameObject)
        {
            var renderer = theGameObject.GetComponent<MeshRenderer>();
            return renderer.bounds.size;
        }
    }
}
