﻿using Assets.Scripts;
using Assets.Scripts.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class ObstacleCreator
    {
        static float _planeWidth = 10f;
        static public List<Obstacle> CreateObstacles(float planceWidth)
        {
            _planeWidth = planceWidth;
            List<Obstacle> obstacleList = new List<Obstacle>();

            int numberOfObstacle = Random.Range(1, 3);

            if (numberOfObstacle == 1)
            {
                var obstacle = CreateOneObstacle();
                obstacleList.Add(obstacle);
            }
            else
            {
                var obstacles = CreateTwoObstacles();
                obstacleList.AddRange(obstacles);
            }

            return obstacleList;
        }

        static Obstacle CreateOneObstacle()
        {
            float xPositionMinForFirst = -_planeWidth / 2;
            float xPositionMaxForFirst = _planeWidth / 2;
            float maxWidthForFirst = _planeWidth;
            return CreateObstacleWithRandomPosition(xPositionMinForFirst, xPositionMaxForFirst, maxWidthForFirst);
        }

        static Obstacle[] CreateTwoObstacles()
        {
            var firstObstacle = CreateOneObstacle();

            var secondObstaclePositionInfo = CalculatePositionForSecondObstacle(firstObstacle);

            var secondObstacle = CreateObstacleWithRandomPosition(secondObstaclePositionInfo.xPositionMinForSecond,
                                                                    secondObstaclePositionInfo.xPositionMaxForSecond,
                                                                    secondObstaclePositionInfo.maxWidthForSecond);

            return new Obstacle[2] { firstObstacle, secondObstacle };
        }

        static (float xPositionMinForSecond, float xPositionMaxForSecond, float maxWidthForSecond) CalculatePositionForSecondObstacle(Obstacle firstObstacle)
        {
            float leftDistance = firstObstacle.X - (firstObstacle.Width / 2) - (-_planeWidth) - GameSettings._MIN_EMPTY_BETWEEN_OBSTACLES;
            float rightDistance = _planeWidth - (firstObstacle.X + (firstObstacle.Width / 2)) - GameSettings._MIN_EMPTY_BETWEEN_OBSTACLES;

            if (leftDistance > rightDistance)
                return CalculatePositionAndWidthAtLeftSide(firstObstacle, leftDistance);
            else 
                return CalculatePositionAndWidthAtRightSide(firstObstacle, rightDistance);

            //float xPositionMinForSecond = -_planeWidth / 2;
            //if (leftDistance < rightDistance)
            //    xPositionMinForSecond = firstObstacle.X + (firstObstacle.Width / 2);

            //float xPositionMaxForSecond = _planeWidth / 2;
            //if (leftDistance > rightDistance)
            //    xPositionMaxForSecond = firstObstacle.X - (firstObstacle.Width / 2);

            //float maxWidthForSecond = Mathf.Max(leftDistance, rightDistance);

            //return (xPositionMinForSecond, xPositionMaxForSecond, maxWidthForSecond);
        }

        static (float xPositionMinForSecond, float xPositionMaxForSecond, float maxWidthForSecond) CalculatePositionAndWidthAtLeftSide(Obstacle firstObstacle, float leftDistance)
        {
            float xPositionMinForSecond = -_planeWidth / 2;
            float xPositionMaxForSecond = firstObstacle.X - (firstObstacle.Width / 2);
            float maxWidthForSecond = leftDistance;

            return (xPositionMinForSecond, xPositionMaxForSecond, maxWidthForSecond);
        }
        static (float xPositionMinForSecond, float xPositionMaxForSecond, float maxWidthForSecond) CalculatePositionAndWidthAtRightSide(Obstacle firstObstacle, float rightDistance)
        {
            float xPositionMinForSecond = firstObstacle.X + (firstObstacle.Width / 2);
            float xPositionMaxForSecond = _planeWidth / 2;
            float maxWidthForSecond = rightDistance;

            return (xPositionMinForSecond, xPositionMaxForSecond, maxWidthForSecond);
        }

        static Obstacle CreateObstacleWithRandomPosition(float xPositionMin, float xPositionMax, float maxWidth)
        {
            float widthRandom = Random.Range(maxWidth / 8, maxWidth / 2);
            float xPositionRandom = Random.Range(xPositionMin + (widthRandom / 2), xPositionMax - (widthRandom / 2));

            return new Obstacle()
            {
                X = xPositionRandom,
                Width = widthRandom,
            };
        }
    }
}
