using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyBarController : MonoBehaviour
{
    Slider _slider;
    Color _lowEnergyColor = Color.red;
    Color _highEnergyColor = Color.green;
    Color _normalEnergyColor = new Color(250f / 255f, 138f / 255f, 22f / 255f);

    // Start is called before the first frame update
    void Start()
    {
        _slider = this.gameObject.GetComponent<Slider>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckKeyEventToUpdateEnergy();
        CheckKeyEventToPreventToUseEnergy();

        AllowUseEnergy();
    }

    void CheckKeyEventToPreventToUseEnergy()
    {
        if (Input.GetKeyUp(GameSettings._KEY_FOR_SHRINK))
        {
            GameController._preventToUseEnergy = true;
        }
    }

    void CheckKeyEventToUpdateEnergy()
    {
        if (Input.GetKey(GameSettings._KEY_FOR_SHRINK) && !GameController._preventToUseEnergy)
        {
            UseEnergy();
        }
        else
        {
            FillEnergy();
        }
    }

    void FillEnergy()
    {
        _slider.value += 0.2f;
    }

    void UseEnergy()
    {
        _slider.value -= 0.5f;
    }

    void AllowUseEnergy()
    {
        if (_slider.value >= _slider.maxValue)
        {
            ChangeFillColor(_highEnergyColor);
            GameController._preventToUseEnergy = false;
        }
        else
        {
            //ChangeFillColor(Color.Lerp(_lowEnergyColor, _normalEnergyColor, _slider.value / _slider.maxValue));
            if (_slider.value > _slider.minValue + GameSettings._MIN_VALUE_TO_ALLOW_USING_ENERGY)
            {
                ChangeFillColor(_normalEnergyColor);
                GameController._preventToUseEnergy = false;
            }
            else
            {
                ChangeFillColor(_lowEnergyColor);
                if (_slider.value <= _slider.minValue)
                {
                    GameController._preventToUseEnergy = true;
                }
            }
        }
    }

    void ChangeFillColor(Color color)
    {
        _slider.gameObject.transform.Find("Fill Area").Find("Fill").GetComponent<Image>().color = color;
    }
}
