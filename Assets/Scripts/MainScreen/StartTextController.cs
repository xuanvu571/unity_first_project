using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartTextController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DestroyText());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator DestroyText()
    {
        yield return new WaitForSeconds(2.0f);
        Destroy(this.gameObject);
    }
}
