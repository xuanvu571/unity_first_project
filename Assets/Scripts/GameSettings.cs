﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public static class GameSettings
    {
        public const KeyCode _KEY_FOR_SHRINK = KeyCode.Space;
        public const float _MIN_VALUE_TO_ALLOW_USING_ENERGY = 20f;
        public const float _MIN_EMPTY_BETWEEN_OBSTACLES = 3f;
        public const float _DELAY_SECOND_FOR_CREATING_OBSTACLES = 4f;
    }
}
